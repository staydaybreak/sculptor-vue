// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuetify from 'vuetify'
import ElementUI from 'element-ui'
import LocaleJAN from 'element-ui/lib/locale/lang/ja'
import App from './App'
import router from './router'

import VuePusher from 'vue-pusher'

import 'element-ui/lib/theme-chalk/index.css'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify)
Vue.use(VuePusher, {
  api_key: '6102d76c6d3d0b1db34d',
  options: {
    cluster: 'ap1',
    encrypted: true
  }
})
Vue.use(ElementUI, { LocaleJAN })
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
