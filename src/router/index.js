import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Build from '@/components/Build'
Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/build',
      name: 'build',
      component: Build
    }
  ]
})
